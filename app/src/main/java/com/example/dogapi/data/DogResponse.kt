package com.example.dogapi.data

data class DogResponse(
    val status: String,
    val message: String
)
