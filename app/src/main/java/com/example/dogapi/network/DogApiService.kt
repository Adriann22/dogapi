package com.example.dogapi.network

// En el paquete network o api
import com.example.dogapi.data.DogResponse
import retrofit2.Call
import retrofit2.http.GET

interface DogApiService {
    @GET("breeds/image/random")
    fun getRandomDogImage(): Call<DogResponse>
}
