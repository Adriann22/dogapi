package com.example.dogapi.network

// En el paquete network o api
import com.example.dogapi.data.DogResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DogApiClient {
    private const val BASE_URL = "https://dog.ceo/api/breeds/image/random"

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val dogApiService = retrofit.create(DogApiService::class.java)

    fun getRandomDogImage(callback: (String?) -> Unit) {
        val call = dogApiService.getRandomDogImage()
        call.enqueue(object : retrofit2.Callback<DogResponse> {
            override fun onResponse(call: Call<DogResponse>, response: retrofit2.Response<DogResponse>) {
                if (response.isSuccessful) {
                    val dogResponse = response.body()
                    callback(dogResponse?.message)
                } else {
                    callback(null)
                }
            }

            override fun onFailure(call: Call<DogResponse>, t: Throwable) {
                callback(null)
            }
        })
    }
}
