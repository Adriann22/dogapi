import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.dogapi.R
import com.example.dogapi.network.DogApiClient
import com.example.dogapi.ui.theme.DogAPITheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DogAPITheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    DogUI()
                }
            }
        }
    }
}

@Composable
fun DogUI() {
    var imageUrl by remember { mutableStateOf<String?>(null) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(
            onClick = {
                DogApiClient.getRandomDogImage { fetchedImageUrl ->
                    imageUrl = fetchedImageUrl
                }
            },
            modifier = Modifier.padding(bottom = 16.dp)
        ) {
            Text("Generar imagen de perro")
        }

        imageUrl?.let { url ->
            Image(
                painter = rememberImagePainter(
                    data = url
                ),
                contentDescription = "Imagen de perro",
                modifier = Modifier.size(300.dp),
                contentScale = ContentScale.FillBounds
            )
        }
    }
}

fun rememberImagePainter(data: String): Painter {

    return TODO("Provide the return value")
}

@Preview(showBackground = true)
@Composable
fun DogUIPreview() {
    DogAPITheme {
        DogUI()
    }
}
